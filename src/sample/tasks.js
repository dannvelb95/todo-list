export default [
    {
        id:0,
        name: 'Task one',
        description: 'Description 1 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',
        status: {
            type: 0,
            description: 'to do'
        }
    },
    {
        id:2,
        name: 'Task two',
        description: 'Description 2 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',
        status: {
            type: 1,
            description: 'in progress'
        }
    },
    {
        id:3,
        name: 'Task three',
        description: 'Description 3 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',

        status: {
            type: 0,
            description: 'to do'
        }
    },
    {
        id:4,
        name: 'Task four',
        description: 'Description 4 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',

        status: {
            type: 0,
            description: 'to do'
        }
    }, {
        id:5,
        name: 'Task five',
        description: 'Description 5 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',

        status: {
            type: 0,
            description: 'to do'
        }
    },
    {
        id:6,
        name: 'Task six',
        description: 'Description 6 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',

        status: {
            type: 2,
            description: 'to do'
        }
    }
]