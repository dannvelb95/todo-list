
import React, { createContext, Component, useState } from 'react';
import tasks from '../sample/tasks'
import App from './views/todo'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import NOTFOUND404 from './views/404'
import Pagina from './views/pagina'
import ApiConsume from './views/apiConsume'
import { NavBar } from './components'
import { theme } from '../app/styles'

const Router = () => {
    const [showMenu, setShowMenu] = useState(false)
    const toggleMenu = () => setShowMenu(!showMenu)
    return <BrowserRouter>
        <div>
            <Switch>
                <Route exact path='/' component={ApiConsume} />
                <Route path='/Pagina1'
                    render={(routeProps) => <Pagina {...routeProps} titleRoute='Pagina1' />}
                />
                <Route path='/Pagina2'
                    render={(routeProps) => <Pagina {...routeProps} titleRoute='Pagina2' />}
                />
                <Route path='/Pagina3'
                    render={(routeProps) => <Pagina {...routeProps} titleRoute='Pagina3' />}
                />
                <Route path='/Pagina4'
                    render={(routeProps) => <Pagina {...routeProps} titleRoute='Pagina4' />}
                />
                <Route component={NOTFOUND404} />
            </Switch>
        </div>
    </BrowserRouter>
}


export default Router;
