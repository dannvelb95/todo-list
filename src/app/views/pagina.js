import React from 'react'
import { theme } from '../styles'

const Pagina = (props)=>{
    return <div style={theme.body}>
        <h1 >{props.titleRoute}</h1>
    </div>
}

export default Pagina