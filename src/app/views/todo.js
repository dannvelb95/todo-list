import React, { createContext, Component } from 'react';
import tasks from '../../sample/tasks'
import {
    TaskItem,
    ContainerTasks,
    TaskInfo,
    NewTask
} from '../components'
import { theme as themeFile, theme2 } from '../styles/index'
import { MyContext, MyProvider } from '../state/context'

class App extends Component {
    constructor() {
        super()
        this.state = {
            tasks,
            theme: themeFile,
            showInfo: false,
            showNew: false,
            itemSelect: {
                name: 'Task one',
                description: 'description',
                status: {
                    type: 0,
                    description: 'to do'
                }
            },
            toggleInfo:this.toggleInfo,
            setItem:this.setItem
        }
    }
    changeTheme = () => {
        this.setState({
            theme: this.state.theme == theme2 ? themeFile : theme2
        })
    }
    toggleInfo = () =>
        this.setState({ showInfo: !this.state.showInfo })

    toggleNew = () => this.setState({ showNew: !this.state.showNew })

    setItem = (item) =>
        this.setState({ itemSelect: item })

    saveNewTask = item => {
        let id = this.state.tasks[this.state.tasks.length - 1].id
        let newTask = {
            id,
            ...item
        }
        this.setState({
            tasks: [...this.state.tasks, newTask]
        })

    }

    updateItem = (prevTask, newTask) => {
        let taskTemp = this.state.tasks
        let index = taskTemp.indexOf(prevTask)
        taskTemp.splice(index, 1, newTask)
        this.setState({ tasks: taskTemp })
    }
    render = () => {
        const { theme, tasks } = this.state
        let todo = tasks.filter(item => item.status.type == 0)
        let inprogress = tasks.filter(item => item.status.type == 1)
        let ready = tasks.filter(item => item.status.type == 2)
        let done = tasks.filter(item => item.status.type == 3)
        return (
            <div style={theme.all}>
                <div style={theme.header}>
                    <p style={theme.titleModal}>React - react-redux - react-saga</p>
                    <button style={theme.buttonSubmit} onClick={this.toggleNew}>new Task</button>
                    <button style={theme.buttonCancel} onClick={this.changeTheme}>Cambiar tema</button>

                </div>
                <div style={theme.body} className="App">
                    <MyProvider defaultValue={this.state} value={this.state}>
                        <ContainerTasks
                            theme={theme}
                            title='To do'
                            items={todo} />
                        <ContainerTasks
                            theme={theme}
                            title='In progress'
                            items={inprogress} />
                        <ContainerTasks
                            theme={theme}
                            title='Ready for Test'
                            items={ready}
                           />
                        <ContainerTasks
                            theme={theme}
                            title='Done'
                            items={done} />
                    </MyProvider>
                </div>
                {this.state.showInfo ?
                    <TaskInfo
                        updateTask={this.updateItem}
                        theme={theme}
                        item={this.state.itemSelect}
                        hide={this.toggleInfo} /> : null}
                {this.state.showNew ?
                    <NewTask
                        hide={this.toggleNew}
                        saveNewTask={this.saveNewTask}
                    /> :
                    null
                }
            </div>
        );
    }
}


export default App;
