import React from 'react'

const NOTFOUND = ()=>{
    return <div>
        <h1>Ruta No encontrada</h1>
    </div>
}

export default NOTFOUND