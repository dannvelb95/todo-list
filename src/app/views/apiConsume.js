import React, { useState } from 'react'
import { theme } from '../styles'
import { useDispatch, useSelector, } from 'react-redux'
import { action as getCollectionApi } from '../state/redux/actionTypes/apiConsume/getVehicles'



const ApiConsume = (props) => {
    const state = useSelector(state => state)
    const dispatch = useDispatch()
    const { apiCollection } = state
    console.log(apiCollection)

    const consumeApiMethod = () => {
        dispatch(getCollectionApi('vehicles'))
    }

    const getVehicles = () => {
        return apiCollection.collection.map((item, index) => <ViewItem key={index} {...item} />)
    }

    return <div style={{ justifyContent: 'center' }}>
        <button onClick={consumeApiMethod}>Get Info Api</button>
        <div>
            {apiCollection.collection  && apiCollection.collection.length>0?
                getVehicles():null
            }
        </div>
    </div>
}

const ViewItem = ({ name, model, passengers }) => {
    return <div style={{ borderStyle: 'solid', borderWidth: 1, margin: 5 }}>
        <p>{name}</p>
        <p>{model}</p>
        <p>{passengers}</p>
    </div>

}



export default ApiConsume