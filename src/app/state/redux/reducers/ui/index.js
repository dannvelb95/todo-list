

import { type as setUiType } from '../../actionTypes/setUi';
import setUiHandler from './setUi';
import { theme  } from '../../../../styles/index'
import { THEME } from '../../const';

const typeToHandler = {
    [setUiType]: setUiHandler,
};

const initialState = {
    [THEME]: theme,
};

const reducer = (state = initialState, action) => {
    const handler = typeToHandler[action.type];

    return handler ? handler(state, action, initialState) : state;
}

export { reducer, initialState };
