import { type as setCollection } from '../../actionTypes/apiConsume/setCollection'

const initialState = {
    collection:[]
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case setCollection:
            return{
                ...state,
                collection:action.data
            }
        default:
            return state
    }
}

export { reducer }