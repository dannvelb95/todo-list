export default (state, data) => {
    let taskTemp = state
    let index = taskTemp.indexOf(data.prevTask)
    taskTemp.splice(index, 1, data.newTask)
    return taskTemp
}