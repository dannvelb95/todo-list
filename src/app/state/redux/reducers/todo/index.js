import { type as addTask } from '../../actionTypes/todo/addTask'
import addTaskHandler from './addTask'

import { type as updateTask } from '../../actionTypes/todo/updateTask'
import updateTaskHandler from './updateTask'


const initialState = []

const typeToHandler = {
    [addTask]: addTaskHandler,
    [updateTask]: updateTaskHandler
}


const reducer = (state = initialState, action) => {
    const handler = typeToHandler[action.type]

    return handler ? handler(state, action, initialState) : state
}

export { reducer, initialState }