import { combineReducers } from 'redux'

import { reducer as todo } from './todo'
import { reducer as ui } from './ui'
import { reducer as apiCollection } from './apiConsume/index'
import storage from 'redux-persist/lib/storage'
import { persistReducer } from 'redux-persist'

const rootReducer = combineReducers({
    ui,
    todo,
    apiCollection: persistReducer({
        key: 'apiCollection',
        storage
    }, apiCollection)
})

export default persistReducer({
    key: 'state',
    storage
}, rootReducer) 