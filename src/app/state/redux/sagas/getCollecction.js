import { takeLatest, put, call, takeEvery } from 'redux-saga/effects'
import { type as getVehicles } from '../actionTypes/apiConsume/getVehicles'
import { getCollection } from './api'
import { action as setCollection } from '../actionTypes/apiConsume/setCollection'

function* getVehiclesAsync(action) {
    const response = yield call(getCollection, action.coleccion)
    yield put(setCollection([...response.data.results]))


}

export default function* getVehiclesSaga() {
    yield takeLatest(getVehicles, getVehiclesAsync)
}

