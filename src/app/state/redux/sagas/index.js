import { all, fork} from 'redux-saga/effects'

import getVehicleSaga from './getCollecction'

export function* rootSaga(){
    yield all([
        fork(getVehicleSaga)
    ])
}