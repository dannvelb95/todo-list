const type = 'SET_COLLECTION'

const action = (data)=>{
    return {
        type,
        data
    }
}

export { type, action}