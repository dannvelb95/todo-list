const type = 'GET_VEHICLES'

const action = (coleccion)=> {
    return {
        type,
        coleccion
    }
}

export {
    type,
    action
}