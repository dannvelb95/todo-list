const type = 'SET_UI'

const action = (key,value) =>{
    return {
        type,
        key,
        value
    }
}

export {
    type,
    action
}