const type = 'ADD_TASK'

const action = (newTask) =>{
    return {
        type,
        newTask
    }
}

export {
    type,
    action
}