const type = 'UPDATE_TASK'

const action = (prevTask,newTask) =>{
    return {
        type,
        prevTask,
        newTask
    }
}

export {
    type,
    action
}