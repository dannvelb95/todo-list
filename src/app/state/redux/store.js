import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './reducers';
import { rootSaga } from './sagas/index'
import createSagaMiddleware from 'redux-saga'
import { persistStore} from 'redux-persist'
import storage from 'redux-persist/lib/storage';

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = typeof window === 'object ' &&
    window._REDUX_DEVTOOLS_EXTENSION_COMPOSE__  ?
    window._REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ({

    }) : compose


const enhacer = composeEnhancers(
    applyMiddleware(sagaMiddleware)
)

const store = createStore(rootReducer,
    enhacer);

    const persistor = persistStore(store)

sagaMiddleware.run(rootSaga)

export { store, persistor };
