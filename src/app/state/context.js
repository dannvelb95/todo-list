import React, { createContext} from 'react'

const MyContext = createContext()

const MyProvider = MyContext.Provider

export {
    MyContext,
    MyProvider
}