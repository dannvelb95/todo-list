const colors = {
    orange: '#BE7C4D',
    black: '#141414',
    white: '#EEF0F2',
    blue: '#0E1116',
    white2: '#FAFFFD',
    purple: '#5F5AA2'
}

const basetheme = {
    all: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        background:colors.white
    }, header: {
        padding: '30px 10px',
    }, body: {
        display: 'flex',
        width: '100%',
        justifyContent:'center',
        padding:'20px 0px',
        margin:'auto',
    }, containerRadio:{
        margin:'5px 20px'
    }
}

export const theme = {
    ...basetheme,
    buttonCancel: {
        width: 70,
        background: 'none',
        border: 'none',
        fontSize: 8,
        borderRadius: 5,
        textAlign: 'right',
        padding: '2px 5px',
        color: 'red',
        cursor: 'pointer',
        float: 'right'
    }, buttonSubmit: {
        width: 70,
        background: colors.purple,
        border: 'none',
        fontSize: 8,
        borderRadius: 5,
        padding: '2px 5px',
        color: '#FFFFFF',
        cursor: 'pointer',
        float: 'right'
    }, titleModal: {
        fontSize: 16,

    }, descriptionModal: {
        fontSize: 10,
        margin: 'auto'
    }, status: {

    }, modal: {
        margin: 'auto',
        background: colors.white2,
        width: '60%',
        padding: 15,
        borderWidth: 4,
        borderStyle: 'solid',
        borderColor: colors.purple,
        borderRadius: 2,
    }, modalContainer: {
        position: 'fixed',
        width: '100%',
        height: '100%',
        top: 0,
        background: 'rgba(1,1,1,.5)',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    status: {
        color: 'green',
        fontSize: 10
    },
    titleItem: {
        fontSize: 10

    },
    subtitleItem: {
        fontSize: 7

    }, statusItem: {
        fontSize: 7

    }, containerItem: {
        background: colors.purple,
        color: '#ffff',
        padding: '1px 6px',
        margin: '9px 0px',
        borderRadius: '5px'
    }, column: {
        width: '25%',
        margin: ' 3px',
        borderWidth: 1,
        borderRadius:5,
        borderColor:colors.purple,
        borderStyle: 'solid',
        padding: '0px 5px',
        maxWidth: 200
    },
    titleColumn:{ 
        fontSize: 12 
    },
    buttonMenu:{
        background:colors.blue,
        color:'#FFFFFF',borderRadius:5,
        padding:5,
        margin:10,
        zIndex:99,
        position:'fixed'
    },
    buttonMenuClose:{
        background:colors.blue,
        color:'#FFFFFF',borderRadius:5,
        padding:5,
        margin:10,
        float:'right',
        zIndex:99999,
    },sideMenu:{
        top:0,
        position:'fixed',
        height:'100%',
        border:'solid',
        borderWidth:2,
        borderColor:colors.orange,
        background:colors.white,
        zIndex:9999,
        width:250
    },
    itemMenu:{
        listStyle:'none',
        color:'#FFFFFF',
        textDecoration:'none',
        padding: '5px 5px',
        margin:'5px auto',
        textStyle:'none',
        width:'90%',
    },
    itemMenuLink:{
        border:'solid',
        borderWidth:'0px 0px 1.5px 0px',
        borderColor:colors.blue,
        color:colors.blue,
        textAlign:'left',
        paddingRight: '15px',
        textDecoration:'none',
        textStyle:'none',
        width:'90%',
        textDecoration:'none',
    },
    list:{
        width:'100%',
        padding: 0,
    }
}

export const theme2 = {
    ...basetheme,
    buttonCancel: {
        width: 70,
        background: 'none',
        border: 'none',
        fontSize: 8,
        borderRadius: 5,
        padding: '2px 5px',
        color: 'red',
        cursor: 'pointer',
        float: 'right'
    }, buttonSubmit: {
        width: 70,
        background: colors.blue,
        border: 'none',
        fontSize: 8,
        borderRadius: 5,
        padding: '2px 5px',
        color: '#FFFFFF',
        cursor: 'pointer',
        float: 'right'
    }, titleModal: {
        fontSize: 16,

    }, descriptionModal: {
        fontSize: 10,
        margin: 'auto'
    }, status: {

    }, modal: {
        margin: 'auto',
        background: colors.white2,
        width: '60%',
        padding: 15,
        borderWidth: 4,
        borderStyle: 'solid',
        borderColor: colors.orange,
        borderRadius: 2,
    }, modalContainer: {
        position: 'fixed',
        width: '100%',
        height: '100%',
        zIndex:99999,
        top: 0,
        background: 'rgba(1,1,1,.5)',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    status: {
        color: 'green',
        fontSize: 10
    },
    titleItem: {
        fontSize: 10

    },
    subtitleItem: {
        fontSize: 7

    }, statusItem: {
        fontSize: 7

    }, containerItem: {
        background: colors.blue,
        color: '#ffff',
        padding: '1px 6px',
        margin: '9px 0px',
        borderRadius: '5px'
    }, column: {
        width: '25%',
        margin: ' 3px',
        borderWidth: 1,
        borderRadius:5,
        borderColor:colors.orange,
        borderStyle: 'solid',
        padding: '0px 5px',
        maxWidth: 200
    },
    titleColumn:{ 
        fontSize: 12 
    },
    buttonMenu:{
        background:colors.purple,
        color:'#FFFFFF',borderRadius:5,
        padding:5,
        margin:10,
        zIndex:9998,
        position:'fixed'
    },
    buttonMenuClose:{
        background:colors.purple,
        color:'#FFFFFF',borderRadius:5,
        padding:5,
        margin:10,
        float:'right',
        zIndex:9998,
    },sideMenu:{
        top:0,
        position:'fixed',
        height:'100%',
        border:'solid',
        borderWidth:2,
        borderColor:colors.purple,
        background:colors.white,
        zIndex:9999,
        width:250
    },
    itemMenu:{
        listStyle:'none',
        color:'#FFFFFF',
        textDecoration:'none',
        padding: '5px 5px',
        margin:'5px auto',
        textStyle:'none',
        width:'90%',
    },
    itemMenuLink:{
        border:'solid',
        borderWidth:'0px 0px 1.5px 0px',
        borderColor:colors.orange,
        color:colors.blue,
        textAlign:'left',
        paddingRight: '15px',
        textDecoration:'none',
        textStyle:'none',
        width:'90%',
        textDecoration:'none',
    },
    list:{
        width:'100%',
        padding: 0,
    }
}

