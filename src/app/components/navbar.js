import React from 'react'
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'
import { theme } from '../styles/index'


const Pagina = (props) => {
    const { show, toggleMenu } = props
    return show ? <div style={theme.sideMenu}>
        <button style={theme.buttonMenuClose} onClick={toggleMenu}>X</button>
        <h1>Navbar</h1>
        <ul style={theme.list}>
            <li style={theme.itemMenu} ><Link style={theme.itemMenuLink} onClick={toggleMenu} to='/'>Home</Link></li>
            <li style={theme.itemMenu} ><Link style={theme.itemMenuLink} onClick={toggleMenu} to='/Pagina1'>Pagina 1</Link></li>
            <li style={theme.itemMenu} ><Link style={theme.itemMenuLink} onClick={toggleMenu} to='/Pagina2'>Pagina 2</Link></li>
            <li style={theme.itemMenu} ><Link style={theme.itemMenuLink} onClick={toggleMenu} to='/Pagina3'>Pagina 3</Link></li>
            <li style={theme.itemMenu} ><Link style={theme.itemMenuLink} onClick={toggleMenu} to='/Pagina4'>Pagina 4</Link></li>
        </ul>
    </div> :
        <button style={theme.buttonMenu} onClick={toggleMenu}>show menu</button>
}

export default Pagina