import React from 'react'
import { theme } from '../styles/index'
import { action as addNewTask } from '../state/redux/actionTypes/todo/addTask'
import { connect } from 'react-redux'

const statusTypes = [{
    type:0,
    description:'to do'
},{
    type:1,
    description:'in progress'
},{
    type:2,
    description:'ready'
},{
    type:3,
    description:'done'
}
]

class NewTask extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            item: {
                name: '',
                description: '',
                status: {
                    type: 0,
                    description: 'to do'
                }
            }
        }
    }

    inputHandle = event => {
        let name = event.target.name
        let value = event.target.value
        if(name=='status') value = statusTypes[value]
        this.setState({
            item: {
                ...this.state.item,
                [name]: value
            }
        })
    }

    save = ()=>{
        this.props.addNewTask(this.state.item)
        this.props.hide()
    }



    render = () => {
        const { item } = this.state
        return <div style={theme.modalContainer}>
            <div style={theme.modal}>
                <h3 >New Task</h3>
                <p>name: </p>
                <input
                    name='name'
                    type='text'
                    onChange={this.inputHandle}
                    value={item.name}
                />
                <p>description: </p>
                <input
                    name='description'
                    type='text'
                    onChange={this.inputHandle}
                    value={item.description}
                />
                <p>description: </p>
                <div style={{ display: 'flex' }}>
                    <div style={theme.containerRadio}>
                        <p>To do</p>
                        <input
                            name='status'
                            type='radio'
                            onChange={this.inputHandle}
                            value={0}
                            checked={item.status.type==0}
                        />
                    </div>
                    <div style={theme.containerRadio}>
                        <p>In progress</p>
                        <input
                            name='status'
                            type='radio'
                            onChange={this.inputHandle}
                            value={1}
                            checked={item.status.type==1}

                        />
                    </div>
                    <div style={theme.containerRadio}>
                        <p>Ready</p>
                        <input
                            name='status'
                            type='radio'
                            onChange={this.inputHandle}
                            value={2}
                            checked={item.status.type==2}

                        />
                    </div>
                    <div style={theme.containerRadio}>
                        <p>Done</p>
                        <input
                            name='status'
                            type='radio'
                            onChange={this.inputHandle}
                            value={3}
                            checked={item.status.type==3}

                        />
                    </div>
                </div>
                <button style={theme.buttonSubmit} onClick={this.save}>save</button>
                <button style={theme.buttonCancel} onClick={this.props.hide}>close</button>
            </div>
        </div>
    }
}

const mapStateToProps = (state)=>{
    return {
        state
    }
}

export default connect(mapStateToProps,{
    addNewTask
}) (NewTask)