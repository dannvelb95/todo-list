import React from 'react'
import { TaskItem } from './'
import { MyContext } from '../state/context'
const ContainerTasks = props => {
  const { items, theme } = props
  const getItems = () =>
    items.map((item, index) =>
      <MyContext.Consumer key={index}>
        {({ toggleInfo, setItem }) =>
          <TaskItem theme={theme} item={item} setItem={setItem} toggleInfo={toggleInfo} />
        }
      </MyContext.Consumer>
    )
  return <div style={theme.column}>
    <p style={theme.titleColumn}>{props.title}</p>
    <div>
      {getItems()}
    </div>
  </div>
}

export default ContainerTasks