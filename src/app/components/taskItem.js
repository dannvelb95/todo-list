import React, { useContext, Component } from 'react'

class TaskItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            item: this.props.item
        }
    }

    setItem = () => {
        const { setItem, item, toggleInfo } = this.props
        setItem(item)
        toggleInfo()
    }

    render = () => {
        const {theme }=this.props
        return <div onClick={this.setItem}  style={theme.containerItem}>
            <p style={theme.titleItem}>{this.state.item.name}</p>
            <p style={theme.subtitleItem}>click for more info</p>
            <p style={theme.statusItem}>{}</p>
        </div>
    }
}

export default TaskItem
