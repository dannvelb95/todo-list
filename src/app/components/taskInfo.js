import React, { Component } from 'react'

const statusTypes = [{
    type: 0,
    description: 'to do'
}, {
    type: 1,
    description: 'in progress'
}, {
    type: 2,
    description: 'ready'
}, {
    type: 3,
    description: 'done'
}
]

class TaskInfo extends Component {
    constructor(props) {
        super(props)//
        this.state = {
            item: this.props.item,
            update: false
        }
    }

    toggleUpdate = () => this.setState({ update: !this.state.update })

    inputHandle = event => {
        let name = event.target.name
        let value = event.target.value
        if (name == 'status') value = statusTypes[value]
        this.setState({
            item: {
                ...this.state.item,
                [name]: value
            }
        })
    }

    save = () => {
        this.props.updateTask(this.props.item,this.state.item)
        this.props.hide()
    }



    render = () => {
        const { theme } = this.props
        const { item } = this.state
        return <div style={theme.modalContainer}>
            <div style={theme.modal}>
                <h3 >Task Information</h3>
                {!this.state.update ?
                    <>
                        <p style={theme.titleModal}>name: {item.name}</p>
                        <p style={theme.descriptionModal}>description: {item.description}</p>
                        <p style={theme.status}>status: {item.status.description}</p>
                        <button style={theme.buttonSubmit} onClick={this.toggleUpdate}>Update</button>
                    </>
                    :
                    <>
                        <p>name: </p>
                        <input
                            name='name'
                            type='text'
                            onChange={this.inputHandle}
                            value={item.name}
                        />
                        <p>description: </p>
                        <input
                            name='description'
                            type='text'
                            onChange={this.inputHandle}
                            value={item.description}
                        />
                        <p>description: </p>
                        <div style={{ display: 'flex' }}>
                            <div style={theme.containerRadio}>
                                <p>To do</p>
                                <input
                                    name='status'
                                    type='radio'
                                    onChange={this.inputHandle}
                                    value={0}
                                    checked={item.status.type == 0}
                                />
                            </div>
                            <div style={theme.containerRadio}>
                                <p>In progress</p>
                                <input
                                    name='status'
                                    type='radio'
                                    onChange={this.inputHandle}
                                    value={1}
                                    checked={item.status.type == 1}

                                />
                            </div>
                            <div style={theme.containerRadio}>
                                <p>Ready</p>
                                <input
                                    name='status'
                                    type='radio'
                                    onChange={this.inputHandle}
                                    value={2}
                                    checked={item.status.type == 2}

                                />
                            </div>
                            <div style={theme.containerRadio}>
                                <p>Done</p>
                                <input
                                    name='status'
                                    type='radio'
                                    onChange={this.inputHandle}
                                    value={3}
                                    checked={item.status.type == 3}

                                />
                            </div>
                        </div>
                        <button style={theme.buttonSubmit} onClick={this.save}>save</button>
                    </>
                }

                <button style={theme.buttonCancel} onClick={this.props.hide}>close</button>
            </div>
        </div>
    }
}


export default TaskInfo
